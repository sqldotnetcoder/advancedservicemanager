﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.ServiceProcess;
using System.Threading;
using System.Text.RegularExpressions ;
using System.Collections.Specialized;
namespace AdvancedServiceManager
{
   

    public partial class FormMain : Form
    {

        protected string MachineName;
        protected string ServiceName;
        protected TreeNode selectedNode;
        private List<string> TotalServers = new List<string>();

        StringCollection serviceNamesCollection;

        protected bool isTray = false; 

        public FormMain()
        {
            InitializeComponent();
           

        }
         
     
  public void InitLoad()
        {

            ChangeTrayState("Refreshing...", Properties.Resources.database_process_true);
 

            serviceNamesCollection = Properties.Settings.Default.ServiceNames;

            if (Environment.GetCommandLineArgs().Length > 1)
            {

                
                this.WindowState = FormWindowState.Minimized;
                this.ShowInTaskbar = false; 


            }
            else
            {
              
               
                this.Visible=true ;
            }


         


            timer1.Interval =(int) Properties.Settings.Default.PollInterval * 1000;


            Thread thread = new Thread(SaveLocalMachineAndLocalServiceFirstTime);
            thread.IsBackground = true;

            thread.Start();

            LoadAllServers();


           
           
        }
    
#region events 

      
          private void timer1_Tick(object sender, EventArgs e)
          {
              Thread pollingThread = new Thread( new ThreadStart( UpdateTrayStatus));
              pollingThread.IsBackground = true;
              pollingThread.Start();
              //UpdateTrayStatus(); 
          }


        private void FormMain_Load(object sender, EventArgs e)
        {
            ///////run from startup 
 
             InitLoad();

       

        }



        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            RestoreWindow();

        }

      

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            MinimizeWindow();

        }
 
        private void treeViewServers_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeViewServers.SelectedNode != null)
                selectedNode = treeViewServers.SelectedNode;
 
          TreeNodeSelect();

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
 
            ChangeServiceStatus(treeViewServers.SelectedNode, ServiceControllerStatus.Running);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
 
            ChangeServiceStatus(treeViewServers.SelectedNode, ServiceControllerStatus.Stopped);
        }
 
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {


            try
            {

                ////////////////////////////////////////////////////Tray icon caused menu to open 
                if (isTray)
                {
                    mnuExit.Visible = true;
                    mnuMonitor.Visible = false;
                    toolStripMenuSep.Visible = true;
                    toolStripMenuItemopen.Visible = true;
                    toolStripSeparatoropen.Visible = true;

                    //UpdateTrayStatus();
                    ChangeUiMenuStatus(serviceController1.Status);
                }
                else/////////////////////////////////////////////////Tree node caused menu to open 
                {
                    mnuExit.Visible = false;
                    toolStripMenuSep.Visible = false;
                    toolStripMenuItemopen.Visible = false;
                    toolStripSeparatoropen.Visible = false;

                    if (treeViewServers.SelectedNode != null && treeViewServers.SelectedNode.Level != 2)
                    {
                        e.Cancel = true;
                    }

                }


            }
            catch { }

        }

        private void treeViewServers_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            isTray = false; 
            if (e.Button == MouseButtons.Right)
                treeViewServers.SelectedNode = treeViewServers.HitTest(e.X, e.Y).Node;
        }




        private void toolStripButtonStart_Click(object sender, EventArgs e)
        {
             
          ChangeServiceStatus(treeViewServers.SelectedNode, sender);
           

        }

        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            ChangeServiceStatus(treeViewServers.SelectedNode, sender);
        }

        private void toolStripButtonPause_Click(object sender, EventArgs e)
        {
            ChangeServiceStatus(treeViewServers.SelectedNode, sender);
        }

        private void toolStripButtonRefresh_Click(object sender, EventArgs e)
        {
            LoadAllServers();

        }


        private void toolStripButtonMonitorInTray_Click(object sender, EventArgs e)
        {
            
            
            UpdateTrayStatus();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                timer1.Stop();
                SaveMonitoredServer();
                this.notifyIcon1.Visible = false;
                this.notifyIcon1.Dispose();
                this.Close();
                this.Dispose();
                Application.Exit();
                GC.Collect();
            }

            catch { }
        }

        private void toolStripButtonOptions_Click(object sender, EventArgs e)
        {
            frmOptions fo = new frmOptions();
            fo.ShowDialog();

            timer1.Interval = fo.Seconds*1000;

            fo.Dispose();

        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            frmAboutBox about = new frmAboutBox();
            about.ShowDialog();
 

        }

        private void toolStripButtonRestart_Click(object sender, EventArgs e)
        {
            ChangeServiceStatus(treeViewServers.SelectedNode, sender);

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmManage frmAddServer = new frmManage();

          if (   frmAddServer.ShowDialog()==DialogResult.OK )  
             LoadAllServers();

        }

        private void mnuStart_Click(object sender, EventArgs e)
        {

            TreeNode selectedNode = FindNodeByMachineAndService();
            ChangeServiceStatus(selectedNode, sender);
        }

        private void mnuStop_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = FindNodeByMachineAndService();
            ChangeServiceStatus(selectedNode, sender);
        }

        private void mnuPause_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = FindNodeByMachineAndService();
            ChangeServiceStatus(selectedNode, sender);
        }

        private void mnuRestart_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = FindNodeByMachineAndService();
            ChangeServiceStatus(selectedNode, sender);
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        private void mnuMonitor_Click(object sender, EventArgs e)
        {
            if (treeViewServers.SelectedNode == null)
                return;

            MachineName = treeViewServers.SelectedNode.Parent.Text;
            ServiceName = ((ServiceData)treeViewServers.SelectedNode.Tag).ServiceName.ToString();

            SaveMonitoredServer();
        }
#endregion
       

#region MyFunctions

        private delegate void SetSQLNodeDelegate(ServiceController sc, TreeNode n);
        private delegate void PerformUiChangeString(String vObject);
        private delegate void PerformUiChange();
        private delegate void PerformUiChangeStatus(ServiceControllerStatus status);
        private delegate void PerformUiChangeNode(TreeNode n);
        private delegate void PerformUiChangeAddChildNode(TreeNode parent, TreeNode child);
        private delegate void PerformUiChangeStringIcon(String text,Icon  icon);


        private void DisableLoadingGui()
        {
            try
            {
                toolStripButton1.Enabled = false;
                toolStripButtonRefresh.Enabled = false;
                 
             
            }

            catch { }
        }
 
        private void DisableGui()
        {
           try
            {
                toolStrip1.Enabled = false;
                treeViewServers.Enabled = false;
                toolStripProgressBar2.Visible = true ;
                notifyIcon1.ContextMenuStrip = null; 
                timer1.Stop();
            }

             catch { }
        }

        private void EnableGui()
        {
            try
            {

                toolStrip1.Enabled = true;
                treeViewServers.Enabled = true;
                toolStripProgressBar2.Visible = false;
                treeViewServers.Nodes[0].Text = "Servers";
                toolStripButtonRefresh.Enabled = true;

                toolStripButton1.Enabled = true;
                notifyIcon1.ContextMenuStrip = contextMenuStrip1;
                contextMenuStrip1.Enabled = true;
                timer1.Start();
                
               Thread updateThread = new Thread(new ThreadStart( UpdateTrayStatus ));
               updateThread.IsBackground = true;
               updateThread.Start();


            }

            catch { }
        }

        private void SortTree()
        {
            treeViewServers.Nodes[0].Expand();
            treeViewServers.Sort(); }


        protected TreeNode FindNodeByMachineAndService()
        {
            try
            {

                foreach (TreeNode serverNode in treeViewServers.Nodes[0].Nodes)
                {
                    if (serverNode.Text == MachineName)
                    {

                        foreach (TreeNode serviceNode in serverNode.Nodes)
                        {

                            if (((ServiceData)serviceNode.Tag).ServiceName == ServiceName)
                            {

                                return serviceNode;

                            }

                        }

                    }

                }
            }

            catch  {  }
          return null;

            
        }
 
 
        /// New thread start ,stop, pause 
        public void ChangeServiceStatusOnThread(object o)
        {

            string messageChanging  = string.Format("Changing service {0} on {1} to {2}", ServiceName, MachineName, ((ThreadState)o).newStatus);
            messageChanging = messageChanging.Replace("100", "Running");
           

            string messageChanged = string.Format("Changed service {0} on {1} to {2}", ServiceName, MachineName, ((ThreadState)o).newStatus);
            messageChanged = messageChanged.Replace("100", "Running");

            this.Invoke(new PerformUiChangeString(SetBalloonInfo), messageChanging);



            ServiceControllerStatus newStatus = ((ThreadState)o).newStatus;
            ServiceController ServiceController = ((ThreadState)o).ServiceController;
            TreeNode n = ((ThreadState)o).node;

            try
            {



                switch (newStatus)
                {
                    case ServiceControllerStatus.Running:
                        {
                            if (ServiceController.Status == ServiceControllerStatus.Stopped)
                                ServiceController.Start();
                            else if (ServiceController.Status == ServiceControllerStatus.Paused)
                                ServiceController.Continue();
                            break;
                        }

                    case ServiceControllerStatus.Stopped:
                        {
                            ServiceController.Stop();
                            break;
                        }

                    case ServiceControllerStatus.Paused:
                        {
                            ServiceController.Pause();
                            break;
                        }

                    default:
                        {
                            if (((Int32)newStatus) == 100)///////////////////////////user clicked restart 
                            {

                                ServiceController.Stop();
                                ServiceController.WaitForStatus(ServiceControllerStatus.Stopped);
                                ServiceController.Start();
                                ServiceController.WaitForStatus(ServiceControllerStatus.Running);

                            }
                            break;
                        }


                }



                if (((Int32)newStatus) < 100)/////////////////////////////////////only if not restarting
                {
                    ServiceController.WaitForStatus(newStatus);

                }
                
                //////////////////////////////////////////////////////////////////Set the new node 
                this.Invoke(new SetSQLNodeDelegate(SetSQLNode), ServiceController, n);

                this.Invoke(new PerformUiChangeString(SetBalloonInfo), messageChanged.Replace("100","Running") );


                this.Invoke(new PerformUiChange(TreeNodeSelect));
 

            }
            catch (Exception ex)
            {
                string ErrorMessage = ex.Message;
                if (ex.InnerException != null)
                    ErrorMessage += ex.InnerException.Message;
               
                this.Invoke(new PerformUiChangeString(SetBalloonError), ErrorMessage);

            }

            finally
            {
                this.Invoke(new PerformUiChange(EnableGui));
            }


  
        }

        /// When clicking start , stop , pause 
        private void ChangeServiceStatus(TreeNode n, object btn)
        {
 
            DisableGui();
            ChangeTrayState("Refreshing...", Properties.Resources.database_process_true);

            string tag = string.Empty;
            if (btn is ToolStripButton)
                tag = ((ToolStripButton)btn).Tag.ToString();
            else if (btn is ToolStripMenuItem)
                tag = ((ToolStripMenuItem)btn).Tag.ToString();

 
            ServiceControllerStatus newStatus = (ServiceControllerStatus)Int32.Parse(tag);

            ThreadState ts = new ThreadState();
            ts.newStatus = newStatus;
            ts.node = n;
            ts.ServiceController = serviceController1;

            serviceController1.ServiceName =  ServiceName;
            serviceController1.MachineName = MachineName;
 
            Thread t = new Thread(ChangeServiceStatusOnThread);
            t.Start(ts);
 
        }


        /// When moving selection from node to node we need to update the button status and context menu status 
        protected void TreeNodeSelect()
        {

            try
            {

                isTray = false;

                if (treeViewServers.SelectedNode == null)
                {
                    DisableServiceControlUI();
                    return;
                }

                if (treeViewServers.SelectedNode.Level < 2)
                {
                    DisableServiceControlUI();
                    return;
                }
                else
                {
                    toolStripButtonMonitorInTray.Enabled = true;
                }



                ServiceControllerStatus status = ((ServiceData)treeViewServers.SelectedNode.Tag).status;


                MachineName = treeViewServers.SelectedNode.Parent.Text;
                ServiceName = ((ServiceData)treeViewServers.SelectedNode.Tag).ServiceName.ToString();

                ChangeUiStatus(status);
                UpdateTrayStatus(status);

            }
            catch { }
        }

        private void ServerListControlUI()
        {
            toolStripButtonRefresh.Enabled = false;
            toolStripButton1.Enabled = false;
           
        }

        private void DisableServiceControlUI()
        {
            toolStripButtonStart.Enabled = false;
            toolStripButtonStop.Enabled = false;
            toolStripButtonPause.Enabled = false;
            toolStripButtonRestart.Enabled = false;
            toolStripButtonMonitorInTray.Enabled = false;

            mnuStop.Enabled = false;
            mnuStart.Enabled = false;
            mnuPause.Enabled = false;
            mnuRestart.Enabled = false;
        }

        private void ChangeUiMenuStatus(ServiceControllerStatus status)
        {


            ///////////////////////////////////////In thie case the service is running 
            if (status == ServiceControllerStatus.Running)
            {
 

                mnuStop.Enabled = true;
                mnuStart.Enabled = false;
                mnuPause.Enabled = true;
                mnuRestart.Enabled = true;



            }

            ///////////////////////////////////////In thie case the service is Stopped 
            if (status == ServiceControllerStatus.Stopped)
            {
 

                mnuStart.Enabled = true;
                mnuStop.Enabled = false;
                mnuPause.Enabled = false;
                mnuRestart.Enabled = false;


                //notifyIcon1.Icon = BitmapToIcon("traystop.ico");
            }

            ///////////////////////////////////////In thie case the service is paused 
            if (status == ServiceControllerStatus.Paused)
            {
 

                mnuStart.Enabled = true;
                mnuStop.Enabled = true;
                mnuPause.Enabled = false;
                mnuRestart.Enabled = true;


            }

            ///////////////////////////////////////Change tray according to the selected node 


        }


        private void ChangeUiStatus( ServiceControllerStatus status)
        {
 

            ///////////////////////////////////////In thie case the service is running 
            if ( status == ServiceControllerStatus.Running)
            {


                toolStripButtonStart.Enabled = false;
                toolStripButtonStop.Enabled = true;
                toolStripButtonPause.Enabled = true;
                toolStripButtonRestart.Enabled = true;

                mnuStop.Enabled = true;
                mnuStart.Enabled = false;
                mnuPause.Enabled = true;
                mnuRestart.Enabled = true;
               


            }

            ///////////////////////////////////////In thie case the service is Stopped 
            if ( status == ServiceControllerStatus.Stopped)
            {


                toolStripButtonStart.Enabled = true;
                toolStripButtonStop.Enabled = false;
                toolStripButtonPause.Enabled = false;
                toolStripButtonRestart.Enabled = false;

                mnuStart.Enabled = true;
                mnuStop.Enabled = false;
                mnuPause.Enabled = false;
                mnuRestart.Enabled = false;


                //notifyIcon1.Icon = BitmapToIcon("traystop.ico");
            }

            ///////////////////////////////////////In thie case the service is paused 
            if ( status == ServiceControllerStatus.Paused)
            {

                toolStripButtonStart.Enabled = true;
                toolStripButtonStop.Enabled = true;
                toolStripButtonPause.Enabled = false;
                toolStripButtonRestart.Enabled = true;

                mnuStart.Enabled = true;
                mnuStop.Enabled = true;
                mnuPause.Enabled = false;
                mnuRestart.Enabled = true;

                
            }

            ///////////////////////////////////////Change tray according to the selected node 

          
        }

        /// //////////////////////////////////////////////This is called in the timer ,also when selected node changes 

        private void UpdateTrayStatus()
        {
         ServiceController sc = new ServiceController(ServiceName, MachineName);
            try
            {

                if (ServiceName == string.Empty)
                {
                    notifyIcon1.Icon = Properties.Resources.database_process_true;
                    return;
                }
                if (MachineName == string.Empty)
                {
                    notifyIcon1.Icon = Properties.Resources.database_process_true;
                    return;
                }


                this.Invoke(new PerformUiChangeStringIcon(ChangeTrayState), string.Format( "Refreshing {0} on {1}", ServiceName , MachineName)  , Properties.Resources.database_process_true);

                Console.WriteLine(ServiceName);
                Console.WriteLine(MachineName);

       
                //sc.Refresh();

                //serviceController1.ServiceName = ServiceName;
                //serviceController1.MachineName = MachineName;
                //serviceController1.Refresh();


                TreeNode serviceNode = FindNodeByMachineAndService();



                ///////////////////////////////////////Check if service node is not null , maybe  tree not fully loaded   
                if (serviceNode != null)
                {
                    this.Invoke(new SetSQLNodeDelegate(SetSQLNode), sc, serviceNode);

                    if (selectedNode == serviceNode)
                    {

                        //////////////////////////////////////////////call this to affect toolbar if the node is selected 

                        this.Invoke(new PerformUiChangeStatus(ChangeUiStatus), sc.Status);

                    }
                    else
                        this.Invoke(new PerformUiChangeStatus(ChangeUiMenuStatus), sc.Status); //ChangeUiMenuStatus(serviceController1.Status);


                }

                UpdateTrayStatus(sc.Status);




            }

            catch (Exception ex) { Console.WriteLine(ex.Message); }
            finally { sc.Dispose(); }

        }


        private void UpdateTrayStatus( ServiceControllerStatus status  )
        {


            string statusText = string.Format("{0}-{1}-{2}", MachineName, ServiceName, status);
            switch (status)
            {

                case ServiceControllerStatus.Running:
                    this.Invoke(new PerformUiChangeStringIcon(ChangeTrayState), statusText, Properties.Resources.database_next_true);
                    break;

                case ServiceControllerStatus.Stopped:
                    this.Invoke(new PerformUiChangeStringIcon(ChangeTrayState), statusText, Properties.Resources.database_remove_true);
                    break;

                case ServiceControllerStatus.Paused:
                    this.Invoke(new PerformUiChangeStringIcon(ChangeTrayState), statusText, Properties.Resources.database_pause__true);
                    break;


            }

        
        }
  

        private void ChangeTrayState( string text , Icon icon   )
        {

            notifyIcon1.Icon = icon;
            notifyIcon1.Text = text;
        }

        private void MinimizeWindow()
        {
             
            this.ShowInTaskbar = false;
            this.Visible = false;
        }

        private void RestoreWindow()
        {
            this.WindowState = FormWindowState.Normal;
            this.Focus();
           
            this.ShowInTaskbar = true;
            this.Visible = true;
        }

        private void LoadAllServers()
        {

            DisableLoadingGui();

            treeViewServers.Nodes[0].Nodes.Clear();
            treeViewServers.Nodes[0].Text = "Loading servers....";
 

            CreateServerNode(Environment.MachineName.ToUpper());

            treeViewServers.Nodes[0].Expand();


            Thread thread = new Thread(LoadAllServersOnThread);
            thread.IsBackground = true;

            thread.Start();


        }

  
        private void LoadAllServersOnThread()
        {
            
 
            TotalServers.Clear();
            TotalServers.Add(Environment.MachineName.ToUpper()); 
            LoadNetworkServersInCollection();
            LoadSavedServersInCollection();

            if (TotalServers.Contains(Environment.MachineName))
                TotalServers.Remove(Environment.MachineName);

            LoadServersFromCollection(TotalServers);

            this.Invoke(new PerformUiChange(EnableGui));
            this.Invoke(new PerformUiChange(SortTree));
            


        }

        private void LoadServersFromCollection(List<string> list)
        {

            foreach (string computer in list)
            {
                TreeNode computerNode = CreateServerNode(computer);
                
            }
        
        
        }

        private void LoadSavedServersInCollection()
        {
 
            if (Properties.Settings.Default.Servers.Contains(Environment.MachineName))
                Properties.Settings.Default.Servers.Remove(Environment.MachineName);
 
            foreach (string ComputerName in Properties.Settings.Default.Servers)
            {
                if ( ComputerName !=null)
                if( !TotalServers.Contains( ComputerName.ToUpper() ) )
                 TotalServers.Add(ComputerName);

            }
 
        }

        private void LoadNetworkServersInCollection()
        {

            NetworkBrowser nb = new NetworkBrowser();
            List<string> listOfNetworkComputers = nb.getNetworkComputers();

            foreach (string networkServer in listOfNetworkComputers)
            {
                if (!TotalServers.Contains(networkServer))
                    TotalServers.Add(networkServer.ToUpper());
            }
             

        }

        protected bool IsSqlService(String fserviceName,String fdisplayName )
        {

            foreach (string iservice in serviceNamesCollection)
            {
 
                if (fserviceName.ToLower().Contains(iservice.ToLower()) | fdisplayName.ToLower().Contains(iservice.ToLower()))
                    return true;

            }

            return false;

        }

        protected void SetComputerNodeUnknown(TreeNode computerNode)
        {

            computerNode.ImageKey = "server_warning.png";
            computerNode.SelectedImageKey = "server_warning.png";
        }

        private void LoadServices(TreeNode computerNode)
        {
             
            

            ServiceController[] scArray = new ServiceController[0];
            try
            {
                scArray = System.ServiceProcess.ServiceController.GetServices(computerNode.Text);
            }
            catch(InvalidOperationException ex )
            {

                this.Invoke(new PerformUiChangeNode(SetComputerNodeUnknown), computerNode);

            
            }
            //TODO catch security exception 

            if (scArray.Length != 0)
            {
                foreach (ServiceController sc in scArray)
                {
                    //////////////////////////////////////////////////////////////Load only SQL services , including DTC
                    if ( IsSqlService( sc.ServiceName , sc.DisplayName ) )
                    {
                        TreeNode serviceNode = new TreeNode(sc.DisplayName);

                        this.Invoke( new PerformUiChangeAddChildNode(AddServiceNode) ,   computerNode, serviceNode);
                        this.Invoke( new  SetSQLNodeDelegate(SetSQLNode), sc, serviceNode );



                    }
                }

                
            }
            
        }

        private void SetSQLNode(ServiceController sc, TreeNode serviceNode)
        {
            bool isLocal = false;
            if (serviceNode.Parent.Text == Environment.MachineName)
                isLocal = true; 

            serviceNode.Tag = new ServiceData(sc.ServiceName, sc.Status , isLocal );
 
            if (sc.Status == ServiceControllerStatus.Running)
            {
                serviceNode.ImageKey = "database_next.png";
                serviceNode.SelectedImageKey = "database_next.png";
            }
            else if (sc.Status == ServiceControllerStatus.Stopped)
            {
                serviceNode.ImageKey = "database_remove.png";
                serviceNode.SelectedImageKey = "database_remove.png";
            }
            else if (sc.Status == ServiceControllerStatus.Paused)
            {
                serviceNode.ImageKey = "databasepause.png";
                serviceNode.SelectedImageKey = "databasepause.png";
            }


           

             
        }

        private   TreeNode CreateServerNode(string computer)
        {
            TreeNode computerNode = new TreeNode(computer);

            computerNode.ImageKey = "server.png";
            computerNode.SelectedImageKey = "server.png";

            this.Invoke(new PerformUiChangeNode(AddComputerNode), computerNode);
 
            LoadServices(computerNode); 
            return computerNode;
        }

        private void AddComputerNode(TreeNode computerNode)
        {

            treeViewServers.Nodes[0].Nodes.Add(computerNode);

        }

        private void AddServiceNode(TreeNode parentNode, TreeNode childNode)
        {

            parentNode.Nodes.Add(childNode);

        }

  
        private void SaveMonitoredServer()
        {
            try
            {
                Properties.Settings.Default.ServiceName = ServiceName;
                Properties.Settings.Default.MachineName = MachineName;
                Properties.Settings.Default.Save();
            }

            catch { }
        }

        protected string GetLocalSqlServer( )
        {

            foreach (ServiceController sc in ServiceController.GetServices(Environment.MachineName))
            {

                if (sc.ServiceName == "MSSQLSERVER" | Regex.IsMatch(sc.ServiceName, @"MSSQL\$.*"))
                    return sc.ServiceName;
 
            }

            return string.Empty;

        
        }


        protected void  SaveLocalMachineAndLocalServiceFirstTime()
        {
            if (Properties.Settings.Default.ServiceName.Length > 0)//////////////already saved before we can use it 
            {
                ServiceName = Properties.Settings.Default.ServiceName;
                MachineName = Properties.Settings.Default.MachineName;



            }

            else/////////////////////////////////////////////////////////////////not saved on this machine before 
            {

                string localSqlservice = GetLocalSqlServer();

                if (localSqlservice.Length > 0)////////////////////////////////////////////////Local SQL server exists 
                {
                    ///////////////////////////////////////////////////////////////////////////Set the global variables 
                    this.MachineName = Environment.MachineName;
                    this.ServiceName = localSqlservice;

                    ///////////////////////////////////////////////////////////////////////////Also save this in settings 

                    SaveMonitoredServer();




                }

                else///////////////////////////////////////////////////////////////////////////No local SQL found 
                {

                    notifyIcon1.Icon = Properties.Resources.database_process_true;
                    notifyIcon1.Text = "No server to monitor";



                }
            }

            UpdateTrayStatus();

            }

        private void SetBalloonInfo(string msg)
        {
            try
            {
                notifyIcon1.ShowBalloonTip(0, "Information", msg, ToolTipIcon.Info);
            }

            catch { }
        }

        protected void SetBalloonError(string errMsg)
        {
            try
            {
                notifyIcon1.ShowBalloonTip(0, "Error", errMsg, ToolTipIcon.Error);
            }
            catch { }
        }

       

#endregion

        private void notifyIcon1_MouseDown(object sender, MouseEventArgs e)
        {
            isTray = true; 
        }

        private void toolStripMenuItemopen_Click(object sender, EventArgs e)
        {
            RestoreWindow();

        }

       
       


    }
}
