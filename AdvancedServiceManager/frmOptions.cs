﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO; 
namespace AdvancedServiceManager
{
    public partial class frmOptions : Form
    {
        public int Seconds
        {

            get { return Int32.Parse(txtSeconds.Text); }
        
        }

        public frmOptions()
        {
            InitializeComponent();
        }

        //private string GetExePath()
        //{
        //    return Path.Combine( Application.StartupPath.ToLower() ,  "AdvancedServiceManager.exe" ) +  " /1" ; 

        //}

        private void frmOptions_Load(object sender, EventArgs e)
        {

            listBoxServiceNames.DataSource = Properties.Settings.Default.ServiceNames;

            try
            {
                string path = Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "SqlTrayManager", "").ToString();

                if (path.ToLower() ==Common.GetExePath().ToLower() )
                    checkBox1.Checked = true;
                else
                    checkBox1.Checked = false;



            }
            catch { }


            txtSeconds.Text = Properties.Settings.Default.PollInterval.ToString();


        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            int mins = 0;
            if (!Int32.TryParse(txtSeconds.Text, out mins))
            {
                MessageBox.Show("Please enter a valid integer");
                return; 
            }
            if (mins < 1 | mins > 60)
            {
                MessageBox.Show("Please enter a valid integer between 1 and 60");
                return;

            }


 

            if (checkBox1.Checked)
               Common.SetLoadOnstartup();
            else
                Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "SqlTrayManager.exe", "");


            Properties.Settings.Default.PollInterval =uint.Parse( txtSeconds.Text) ;
            Properties.Settings.Default.Save();

            this.Close();
            
             
        }
    }
}
