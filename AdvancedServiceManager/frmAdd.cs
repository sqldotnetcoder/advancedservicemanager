﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections.Specialized;
namespace AdvancedServiceManager
{
 
    public partial class frmManage : Form
    {

        //public string serverName; 

        public frmManage()
        {
            InitializeComponent();
        }

       
        private void btnOk_Click(object sender, EventArgs e)
        {

            StringCollection  sc = new StringCollection( )  ; 
            foreach ( ListViewItem lvi in listView1.Items) 
            {
                sc.Add(lvi.Text); 
 
            }

            string[] sa = new string[sc.Count];
            sc.CopyTo(sa, 0);
            Common.SaveServerList(sa);

            this.Close();

        }

        private void frmAdd_Load(object sender, EventArgs e)
        {
            foreach (string server in Common.GetServerList())
            {

                listView1.Items.Add(server,  "server.ico");
            
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
                return;

            listView1.Items.Remove(listView1.SelectedItems[0]);
 

        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string serverName = textBoxServer.Text.Trim().ToUpper();



            if (serverName.Length == 0)
            {
                errorProvider1.SetError(textBoxServer, "Please provide a server name or ip");
                return;
            }


            if (listView1.Items.ContainsKey(textBoxServer.Text))
            {
                errorProvider1.SetError(textBoxServer, "This server is already added");
                return;
            }



            ListViewItem lvi = new ListViewItem(serverName, "server.ico");

            lvi.Name = serverName;
            listView1.Items.Add ( lvi ) ;
            textBoxServer.Text = string.Empty; 
        }

       
    }
}
