﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceProcess;
using System.Collections.Specialized;
using System.IO ; 
 

namespace AdvancedServiceManager
{
    class Common
    {
          public static string GetExePath()
        {
            return Path.Combine(System.Windows.Forms.Application.StartupPath.ToLower(), "SqlTrayManager.exe") + " /1"; 

        }

        public static bool AddServer(string serverName)
        {
            serverName = serverName.ToLower();
            if (Properties.Settings.Default.Servers.Contains(serverName) == false)
            {
                Properties.Settings.Default.Servers.Add(serverName );
                Properties.Settings.Default.Save();
                return true; 
            }
            else
            {

                return false ; 
            }

        }


        public static void DeleteServer(string serverName)
        {

            if (Properties.Settings.Default.Servers.Contains(serverName) == true)
            {
                Properties.Settings.Default.Servers.Remove(serverName);
                Properties.Settings.Default.Save();
            }
        }


        public static StringCollection GetServerList( )
        {

            return Properties.Settings.Default.Servers;
            
        }


        public static void SaveServerList(string[] servers)
        {

            Properties.Settings.Default.Servers.Clear();
            Properties.Settings.Default.Servers.AddRange( servers  );
            Properties.Settings.Default.Save();

        }

        public static void SetLoadOnstartup() 
        {

            Microsoft.Win32.Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "SqlTrayManager", GetExePath());
        
        }


        public static void LogError(Exception ex)
        {

            try
            {
                System.Diagnostics.EventLog.WriteEntry("AdvancedServiceManager", ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
            catch { }
        
        }

    }
}
